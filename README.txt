[���]
========================================================================================
������! ����� �� �������, ��� ��� ������ ��������.
������ ��������� ��������� ��������� � ��� �������� � ����.
���� ��������, ��� � ������ ������������� ������ �������� � ����� ������������ ����� ��������,
����������� ������ �� �����, � ���� ������ ��� ����� ���������� �� ������������� ������ ��������!

CTFAK: https://github.com/CTFAK/CTFAK
Anaconda: https://github.com/fnmwolf/Anaconda/
Python: https://www.python.org/
NWJS: https://nwjs.io/
�������� ��� ���� ���������: https://gitlab.com/TheStazik/unpack-box/
�������� �� GameJolt: https://gamejolt.com/games/UnpackBox/833282

� �� ������� ���������� ������ ��������, ������ ����� ��������! �������, ��� ���������
========================================================================================




[ENG]
========================================================================================
Hi! I would like to say that this is just a shell.
This program downloads programs and already works with them.
I want to note that I am against using these programs for the purpose 
of researching other people's programs, use only on your own, otherwise 
you should refuse to use these programs!

CTFAK: https://github.com/CTFAK/CTFAK
Anaconda: https://github.com/fnmwolf/Anaconda/
Python: https://www.python.org/
NWJS: https://nwjs.io/
The source code of this program: https://gitlab.com/TheStazik/unpack-box/
Page on GameJolt: https://gamejolt.com/games/UnpackBox/833282

I am not the owner of these programs, only the shell itself! Thank you for reading
========================================================================================